﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Ranger : Hero
    {
        public Ranger(string name, int level) : base(name, level)
        {
            Attributes = new BaseAttributes(0, 2, 0) + new BaseAttributes(1, 5, 1) * level;
        }

        /// <summary>
        /// Checks if this class can wear this armor
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>bool</returns>
        public override bool ArmorTest(Armor armor)
        {
            return (armor.ArmorType == ArmorType.ARMOR_LEATHER || armor.ArmorType == ArmorType.ARMOR_MAIL);
        }

        /// <summary>
        /// Checks if this class can wear this weapon
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>bool</returns>
        public override bool WeaponTest(Weapon weapon)
        {
            return (weapon.WeaponType == WeaponType.WEAPON_BOW);
        }

        
        /// /// <summary>
        /// Returns the primary attribute of this class
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns>int</returns>
        public override int Primary(BaseAttributes attribute)
        {
            return attribute.Dexterity;
        }
    }
}
