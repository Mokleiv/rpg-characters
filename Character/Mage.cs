﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Mage : Hero
    {
        public Mage(string name, int level) : base(name, level)
        {
            Attributes = new BaseAttributes(0,0,3) + new BaseAttributes(1, 1, 5) * level;
        }

        /// <summary>
        /// Checks if this class can wear this armor
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>bool</returns>
        public override bool ArmorTest(Armor armor)
        {
            return (armor.ArmorType == ArmorType.ARMOR_CLOTH);
        }

        /// <summary>
        /// Returns the primary attribute of this class
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns>int</returns>
        public override int Primary(BaseAttributes attribute)
        {
            return attribute.Intelligence;
        }

        /// <summary>
        /// Checks if this class can wear this weapon
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>bool</returns>
        public override bool WeaponTest(Weapon weapon)
        {
            return (weapon.WeaponType == WeaponType.WEAPON_STAFF || weapon.WeaponType == WeaponType.WEAPON_WAND);
        }
    }
}
