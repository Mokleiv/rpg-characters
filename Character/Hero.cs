﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public abstract class Hero
    {
        public string Name;
        public int Level;
        public Dictionary<EquipmentType, Item> CharacterSlots = new();

        public BaseAttributes Attributes;

        public Hero(String name, int level)
        {
            Name = name;
            Level = level;
        }

        /// <summary>
        /// Will try to equip an item to a character. The item is stored in a dictionary.
        /// </summary>
        /// <param name="equipmentType"></param>
        /// <param name="item"></param>
        /// <returns>String</returns>
        /// <exception cref="InvalidWeaponException"></exception>
        /// <exception cref="InvalidArmorException"></exception>
        public string EquipItem(EquipmentType equipmentType, Item item)
        {

            if (item.GetType() == typeof(Weapon))
            {
                if (!WeaponTest((Weapon)item))
                {
                    throw new InvalidWeaponException();
                }
            }

            if (item.GetType() == typeof(Armor))
            {
                if (!ArmorTest((Armor)item))
                {
                    throw new InvalidArmorException();
                }
            }


            if (item.ItemLevel > Level)
            {
                if (item is Weapon)
                {
                    throw new InvalidWeaponException();
                }
                else
                {
                    throw new InvalidArmorException();
                }
            }

            CharacterSlots.Add(equipmentType, item);

            if (item.GetType() == typeof(Armor))
            {
                return "New armour equipped!";
            }

            return "New weapon equipped!";
        }

        /// <summary>
        /// Calculates the total damage of a character based on equipment and attributes
        /// Checks if a character has a weapon equiped
        /// </summary>
        /// <returns>Double</returns>
        public double CharacterDamage()
        {
            BaseAttributes totalAttributes = new BaseAttributes(0, 0, 0);
            foreach (var item in CharacterSlots)
            {
                Armor armor1 = item.Value as Armor;
                if (armor1 != null)
                {
                    totalAttributes += armor1.BaseAttributes;
                }
            }

            totalAttributes += Attributes;
            double damageAttribute = Primary(totalAttributes);

            if (!CharacterSlots.ContainsKey(EquipmentType.EQUIPMENT_WEAPON))
            {   // 1 * (1 + (5 / 100)) (1 + damageAttribute / 100)
                return 1 * (1 + (5 / 100));
            }

            Weapon weapon = (Weapon)CharacterSlots[EquipmentType.EQUIPMENT_WEAPON];

            double weaponDPS = weapon.CalcDamage(weapon.WeaponAttributes);

            return weaponDPS * (1 + (damageAttribute / 100));
        }

        /// <summary>
        /// Simply returns a string with all character data
        /// </summary>
        /// <returns>String</returns>
        public string CharacterPrint()
        {
            return $" Character name: {Name}\n Character level: {Level}\n Strength: {Attributes.Strength}\n Dexterity: {Attributes.Dexterity}\n Intelligence: {Attributes.Intelligence} \n Damage: {CharacterDamage()}";
        }
        abstract public int Primary(BaseAttributes attribute);

        abstract public bool WeaponTest(Weapon weapon);
        abstract public bool ArmorTest(Armor armor);
    }
}
