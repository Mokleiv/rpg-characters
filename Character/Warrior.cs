﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Warrior : Hero
    {
        public Warrior(string name, int level) : base(name, level)
        {
            Attributes = new BaseAttributes(2, 0, 0) + new BaseAttributes(3, 2, 1) * level;
        }

        /// <summary>
        /// Checks if this class can wear this armor
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>bool</returns>
        public override bool ArmorTest(Armor armor)
        {
            return (armor.ArmorType == ArmorType.ARMOR_MAIL || armor.ArmorType == ArmorType.ARMOR_PLATE);
        }

        /// <summary>
        /// Checks if this class can wear this weapon
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>bool</returns>
        public override bool WeaponTest(Weapon weapon)
        {
            return (weapon.WeaponType == WeaponType.WEAPON_AXE
                || weapon.WeaponType == WeaponType.WEAPON_HAMMER
                || weapon.WeaponType == WeaponType.WEAPON_SWORD);
        }

        /// /// <summary>
        /// Returns the primary attribute of this class
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns>int</returns>
        public override int Primary(BaseAttributes attribute)
        {
            return attribute.Strength;
        }
    }
}
