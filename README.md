# Assignment 1: C#-Fundamentals

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

C# Console Application to create RPG Characters.

## Table of Contents

- [Install](#install)
- [Maintainers](#maintainers)
- [License](#license)

## Install

```
Clone the Repo and run it in your IDE of choice.
```

## Maintainers

[@Mokleiv](https://github.com/Mokleiv)

## License

MIT © 2022 Sondre Mokleiv Nygård
