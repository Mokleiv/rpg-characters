﻿using System;


namespace RPGCharacters
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Test data
            Warrior testWarrior = new Warrior("Geir", 1);
            Weapon textAxe = new Weapon("Common Axe", 1, EquipmentType.EQUIPMENT_WEAPON)
            {
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor testArmor = new Armor("Plate Armor", 1, EquipmentType.EQUIPMENT_BODY)
            {
                ArmorType = ArmorType.ARMOR_PLATE,
                BaseAttributes = new BaseAttributes() { Strength = 1 }
            };
            testWarrior.EquipItem(EquipmentType.EQUIPMENT_BODY, testArmor);
            testWarrior.EquipItem(EquipmentType.EQUIPMENT_WEAPON, textAxe);

            Console.WriteLine(testWarrior.CharacterPrint());
        }
    }
}
