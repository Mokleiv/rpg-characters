using System;
using Xunit;
using RPGCharacters;

namespace RPGCharactersTests
{
    public class EquipmentTest
    {
        [Fact]
        public void EquipmentTest1()
        {
            // Arrange Expected Result
            Warrior testWarrior = new Warrior("Geir", 1);
            Weapon textAxe = new Weapon("Common Axe", 2, EquipmentType.EQUIPMENT_WEAPON)
            {
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            // Act Actual Result
            Action actual = () => testWarrior.EquipItem(EquipmentType.EQUIPMENT_WEAPON, textAxe);

            // Assert Do Test
            Assert.Throws<InvalidWeaponException>(actual);
        }
        [Fact]
        public void EquipmentTest2()
        {
            // Arrange Expected Result
            Warrior testWarrior = new Warrior("Geir", 1);
            Armor testArmor = new Armor("Plate Armor", 2, EquipmentType.EQUIPMENT_BODY)
            {
                ArmorType = ArmorType.ARMOR_PLATE,
                BaseAttributes = new BaseAttributes() { Strength = 1 }
            };

            // Act Actual Result
            Action actual = () => testWarrior.EquipItem(EquipmentType.EQUIPMENT_BODY, testArmor);

            // Assert Do Test
            Assert.Throws<InvalidArmorException>(actual);
        }
        [Fact]
        public void EquipmentTest3()
        {
            // Arrange Expected Result
            Warrior testWarrior = new Warrior("Geir", 1);
            Weapon testBow = new Weapon("Common Bow", 1, EquipmentType.EQUIPMENT_WEAPON)
            {
                WeaponType = WeaponType.WEAPON_BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            // Act Actual Result
            Action actual = () => testWarrior.EquipItem(EquipmentType.EQUIPMENT_WEAPON, testBow);

            // Assert Do Test
            Assert.Throws<InvalidWeaponException>(actual);
        }
        [Fact]
        public void EquipmentTest4()
        {
            // Arrange Expected Result
            Warrior testWarrior = new Warrior("Geir", 1);
            Armor testArmor = new Armor("Cloth Armor", 1, EquipmentType.EQUIPMENT_BODY)
            {
                ArmorType = ArmorType.ARMOR_CLOTH,
                BaseAttributes = new BaseAttributes() { Intelligence = 1 }
            };

            // Act Actual Result
            Action actual = () => testWarrior.EquipItem(EquipmentType.EQUIPMENT_WEAPON, testArmor);

            // Assert Do Test
            Assert.Throws<InvalidArmorException>(actual);
        }
        [Fact]
        public void EquipmentTest5()
        {
            // Arrange Expected Result
            Warrior testWarrior = new Warrior("Geir", 1);
            Weapon textAxe = new Weapon("Common Axe", 1, EquipmentType.EQUIPMENT_WEAPON)
            {
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            string expected = "New weapon equipped!";

            // Act Actual Result
            string actual = testWarrior.EquipItem(EquipmentType.EQUIPMENT_WEAPON, textAxe);

            // Assert Do Test
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipmentTest6()
        {
            // Arrange Expected Result
            Warrior testWarrior = new Warrior("Geir", 1);
            Armor testArmor = new Armor("Plate Armor", 1, EquipmentType.EQUIPMENT_BODY)
            {
                ArmorType = ArmorType.ARMOR_PLATE,
                BaseAttributes = new BaseAttributes() { Strength = 1 }
            };
            string expected = "New armour equipped!";

            // Act Actual Result
            string actual = testWarrior.EquipItem(EquipmentType.EQUIPMENT_BODY, testArmor);

            // Assert Do Test
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipmentTest7()
        {
            // Arrange Expected Result
            Warrior testWarrior = new Warrior("Geir", 1);
            double expected = 1 * (1 + (5 / 100));

            // Act Actual Result
            double actual = testWarrior.CharacterDamage();

            // Assert Do Test
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipmentTest8()
        {
            // Arrange Expected Result
            Warrior testWarrior = new Warrior("Geir", 1);
            Weapon textAxe = new Weapon("Common Axe", 1, EquipmentType.EQUIPMENT_WEAPON)
            {
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            testWarrior.EquipItem(EquipmentType.EQUIPMENT_WEAPON, textAxe);
            double expected = (7 * 1.1) * (1 + (5 / 100.0));

            // Act Actual Result
            double actual = testWarrior.CharacterDamage();

            // Assert Do Test
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void EquipmentTest9()
        {
            // Arrange Expected Result
            Warrior testWarrior = new Warrior("Geir", 1);
            Weapon textAxe = new Weapon("Common Axe", 1, EquipmentType.EQUIPMENT_WEAPON)
            {
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor testArmor = new Armor("Plate Armor", 1, EquipmentType.EQUIPMENT_BODY)
            {
                ArmorType = ArmorType.ARMOR_PLATE,
                BaseAttributes = new BaseAttributes() { Strength = 1 }
            };
            testWarrior.EquipItem(EquipmentType.EQUIPMENT_WEAPON, textAxe);
            testWarrior.EquipItem(EquipmentType.EQUIPMENT_BODY, testArmor);
            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100.0));


            // Act Actual Result
            double actual = testWarrior.CharacterDamage();

            // Assert Do Test
            Assert.Equal(expected, actual);
        }
    }
}
