﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public enum EquipmentType
    {
        EQUIPMENT_HEAD,
        EQUIPMENT_BODY,
        EQUIPMENT_LEGS,
        EQUIPMENT_WEAPON,
    }
}
