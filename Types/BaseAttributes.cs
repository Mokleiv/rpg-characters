﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public struct BaseAttributes
    {
        public int Strength, Dexterity, Intelligence;

        public BaseAttributes(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        /// <summary>
        /// Operator function to add attributes
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>BaseAttributes</returns>
        public static BaseAttributes operator +(BaseAttributes a, BaseAttributes b)
        {
            return new BaseAttributes(a.Strength + b.Strength,a.Dexterity + b.Dexterity, a.Intelligence + b.Intelligence);
        }

        /// <summary>
        /// Operator function to mulitiply attributes.
        /// Used when a characters levels up
        /// </summary>
        /// <param name="a"></param>
        /// <param name="multiplier"></param>
        /// <returns>BaseAttributes</returns>
        public static BaseAttributes operator *(BaseAttributes a, int multiplier)
        {
            return new BaseAttributes(a.Strength * multiplier, a.Dexterity * multiplier, a.Intelligence * multiplier);
        }
    }
}
