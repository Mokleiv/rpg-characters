﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Armor : Item
    {
        public ArmorType ArmorType { get; set; }
        public BaseAttributes BaseAttributes { get; set; }

        public Armor(string itemName, int itemLevel, EquipmentType equipmentType) : base(itemName, itemLevel, equipmentType)
        {
        }
    }
}
