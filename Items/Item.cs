﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public abstract class Item
    {
        public string ItemName;
        public int ItemLevel;
        public EquipmentType EquipmentType;

        public Item(string itemName, int itemLevel, EquipmentType equipmentType)
        {
            ItemName = itemName;
            ItemLevel = itemLevel;
            EquipmentType = equipmentType;
        }
    }
}
