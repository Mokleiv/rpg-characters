﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Weapon : Item
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }
        public Weapon(string itemName, int itemLevel, EquipmentType equipmentType) : base(itemName, itemLevel, equipmentType)
        {
        }

        /// <summary>
        /// Calculates the DPS for this weapon
        /// </summary>
        /// <param name="attributes"></param>
        /// <returns>double</returns>
        public double CalcDamage(WeaponAttributes attributes)
        {
            return attributes.Damage * attributes.AttackSpeed;
        }

    }
}
